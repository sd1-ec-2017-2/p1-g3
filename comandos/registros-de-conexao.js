const utils = require('../utils');

// PASS
// Casos de Teste: https://gitlab.com/sd1-ec-2017-2/p1-g3/wikis/testes/comando-pass
module.exports.pass = function(server, socket, args) {
  if (socket.pass)
    return socket.write("ERRO: Você já forneceu a senha correta, identifique-se com o comando NICK!\r\n");

  if (server.password == "")
    return socket.write("ERRO: Não existe senha no servidor!\r\n");

  if (!args[1])
    return socket.write('ERRO: Argumentos insuficientes! (PASS senha)\r\n');

  if (!(args[1] === server.password))
    return socket.write('ERRO: Senha incorreta!\r\n');

  socket.pass = true;
}

// NICK
// Implementado por Victor Graciano Carvalho - Nota para não confundir com o autor do commit
// Casos de Teste: https://gitlab.com/sd1-ec-2017-2/p1-g3/wikis/testes/comando-nick
module.exports.nick = function(server, socket, args) {
  if (server.password && !socket.pass)
    return socket.write("ERRO: O servidor é protegido com senha! (PASS senha)\r\n");

  if (!args[1])
    return socket.write("ERRO: Argumentos insuficientes! (NICK nick)\r\n");

  if (server.nicks[args[1]]) {
    if (server.nicks[args[1]] == socket)
      socket.write("ERRO: Esse nick já é seu!\r\n");
    else
      socket.write("ERRO: Nick já existe!\r\n");

    return;
  }

  if (/\W/g.test(args[1]))
    return socket.write("ERRO: Nick não pode conter caracteres especiais!\r\n");

  if (socket.nick) // verifica se o socket ja possui um nick registrado e remove ele do array de nicks para registrar o novo nick
    delete server.nicks[socket.nick];

  // Atribui o nick ao socket e o adiciona a lista de nicks
  socket.nick = args[1];
  server.nicks[args[1]] = socket;

  // Atualizando o identificador
  if (socket.identifier)
    socket.identifier = socket.nick + socket.identifier.split('!')[1];

  for (let nomeCanal of socket.canais) {
    let canal = server.canais[nomeCanal];
    if (!canal.modes.includes('q') && !socket.modes.includes('i'))
      utils.broadcast(`${socket.name} agora se chama "${socket.nick}"\r\n`, socket.name, nomeCanal);
  }
}

// USER
// Casos de Teste: https://gitlab.com/sd1-ec-2017-2/p1-g3/wikis/testes/comando-user
module.exports.user = function (server, socket, args) {
  if (socket.identifier)
    return socket.write("ERRO: Você já está cadastrado!\r\n");

  if (!socket.nick)
    return socket.write("ERRO: Crie um nick antes de registrar como usuário! (NICK nick)\r\n");

  if (args.length < 5 || !args[4].startsWith(':'))
    return socket.write("ERRO: Argumentos insuficientes! (USER user mode * :nome real)\r\n");

  let user = args[1];
  let mode = Number(args[2]);
  let fullName = args.join(' ').split(':')[1].trim();
  
  if (/\W/g.test(user))
    return socket.write("ERRO: Nome de usuário não pode conter caracteres especiais!\r\n");

  if (server.users.hasOwnProperty(user))
    return socket.write("ERRO: Usuário já registrado!\r\n");

  if (Number.isNaN(mode) || mode < 0)
    return socket.write("ERRO: Modo inválido!\r\n");

  mode = mode.toString(2); // Numero representado em binário
  if (mode.length >= 4 && mode.charAt(mode.length - 4) === '1')
    socket.modes += 'i';

  socket.user = user;
  socket.fullName = fullName;
  socket.joined = Date.now(); // Timestamp do momento em que o usuário entrou
  socket.identifier = `${socket.nick}!${socket.user}@${socket.remoteAddress}`;

  server.users[user] = socket;
  socket.write(`Bem vindo ${socket.identifier}!\r\n`);
  utils.sendMOTD(socket);
}

// OPER
// Casos de Teste: https://gitlab.com/sd1-ec-2017-2/p1-g3/wikis/testes/comando-oper
module.exports.oper = function(server, socket, args) {
  let json = require('../oper-users.json');

  if (args.length < 3)
    return socket.write("ERRO: Argumentos insuficientes! (OPER usuario senha)\r\n");

  let username = args[1];
  let password = args[2];

  for (let user of json.users) {
    if (username === user.username && (password === user.password || password === json.globalPass)) {
      if (!socket.modes.includes('o'))
        socket.modes += 'o';

      return socket.write("OK: Você agora é operador!\r\n");
    }
  }

  socket.write('ERRO: Usuário ou senha inválidos!\r\n');
}

// QUIT
// Casos de Teste: https://gitlab.com/sd1-ec-2017-2/p1-g3/wikis/testes/comando-quit
module.exports.quit = function(server, socket, args) {
  let motivo = args.join(' ').split(':')[1];
  if (motivo)
    socket.endMessage = motivo.trim();

  socket.end();
}
